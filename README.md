# Article API #

This is a simple node application to expose a basic API

### Install ###

Install NPM dependencies through:

```
npm install
```

### Usage ###

Run application with:

```
npm run dev
```