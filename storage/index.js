const data = require('./dummy_data');

/**
 * A simple data storage wrapper. A different provider can easily
 * be added without having to change code in the app.
 */
module.exports = {
  all: (model) => {
    if (model === 'articles') {
      return data;
    }

    throw Error(`Model '${model}' does not exist.`);
  },
  single: (model, id) => {
    // NYI - Find a single item and return it.
  },
  update: (model, data) => {
    // NYI - Update an item if id is supplied in data, otherwise insert new.
  },
  delete: (model, id) => {
    // NYI - Delete an item.
  },
};
