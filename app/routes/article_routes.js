module.exports = (app, db) => {
  app.get('/articles', (request, response) => {
    response.send(db.all('articles'));
  });
};
