const articleRoutes = require('./article_routes');

module.exports = (app, db) => {
  articleRoutes(app, db);
};
